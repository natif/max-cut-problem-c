// MaxCut2Algorithm.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include<iostream>
#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<ctime>
#include<vector>

#define VERTICALS 6
#define EDGES 14

struct Edge {
    int vert1,vert2;
};

using namespace std;

//Jak zmaksymalizowac liczbe krawedzi pomiedzy V1 a V2?

int checkForMaxCuts(vector<int> v1, vector<int> v2,Edge edges[]);

int _tmain(int argc, _TCHAR* argv[])
{
	srand(time(NULL));
	int max_cut = 0;
	int iteration = 0;
	vector<int> v1;
	vector<int> v2;
	
	vector<int> v1_saved;
	vector<int> v2_saved;
	
	Edge edges[EDGES]= {
        0,1,
        1,0,
        1,2,
        2,1,
        2,3,
        3,2,
        2,4,
        4,2,
        2,5,
        5,2,
        4,5,
        5,4,
        3,5,
        5,3
    };
    
    int verticals[VERTICALS];
    for(int k = 0; k < VERTICALS; k++) {
		verticals[k] = 0;
	}	
    
	cout<<"How many times look for max_cut? "<<endl;
	cin>>iteration;
	
	for(int v = 0; v < iteration; v++) {
		
		v1.clear();
		v2.clear();
		
		int actual_max_cut = 0;
		for(int k = 0; k < VERTICALS; k++) {
    		verticals[k] = 0;
    	}
		
		for(int j = 0; j < VERTICALS; j++) {
			verticals[j] = rand()%2;	
		}
		
		for(int j = 0; j < VERTICALS; j++) {
		//	cout<<"\nVERTICALS "<<verticals[j];
		}
		
		//v1 keeps those with 1, v2 keeps those verts with 0;
		for(int j = 0; j < VERTICALS; j++) {
			if(verticals[j] == 1) {
				v1.push_back(j);
			} else {
				v2.push_back(j);
			}	
		}
			
		actual_max_cut = checkForMaxCuts(v1,v2,edges);	
		
		if(actual_max_cut > max_cut) {
			v1_saved.clear();
			v2_saved.clear();
			
			max_cut = actual_max_cut;
			v1_saved = v1;
			v2_saved = v2;
		}
		
		//cout<<"Actual cut "<<actual_max_cut<<endl<<endl;

		
	}	

	for(int i = 0; i < v1_saved.size(); i++) {
		cout<<"\nVert 1: " << v1[i] <<endl<<endl;
	}
	
	for(int i = 0; i < v2_saved.size(); i++) {
		cout<<"\nVert 2: " << v2[i] <<endl<<endl;	
	}
	
	cout<<"Max cut "<<max_cut<<endl<<endl;
	system("pause");	
	return 0;
}

int checkForMaxCuts(vector<int> v1, vector<int> v2, Edge edges[]) {
	int counter = 0;
	//v1 keeps those verts with value 1
	for(int i = 0; i < v1.size(); i++) {
		for(int j = 0; j < v2.size(); j++) {
			
			for(int k = 0; k < EDGES; k++) {
				if(edges[k].vert1 == v1[i] && edges[k].vert2 == v2[j]) {
					counter++;
				}	
			}	
		}
	}	
	return counter;
}

